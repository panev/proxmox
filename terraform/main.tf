provider "proxmox" {
  pm_api_url          = "https://pve.sysops.app:8006/api2/json"
  pm_api_token_id     = "api@pam!api"
  pm_api_token_secret = "b4e87191-ffee-........................"
  pm_tls_insecure     = true
#  pm_debug            = true
  pm_log_enable = true
  pm_log_file = "terraform-plugin-proxmox.log"
  pm_log_levels = {
    _default = "debug"
    _capturelog = ""
  }
}

resource "proxmox_vm_qemu" "test_server" {
  vmid        = 0
  name        = "test-vm" # var.vm_name
  desc        = "A test VM for PXE boot mode."
  target_node = "pve"                # var.target_node
  clone       = "ubuntu-22.04-cloud" # var.template_name
  full_clone  = true
  agent       = 1
  os_type     = "cloud-init"
  cores       = 2
  sockets     = 1
  cpu         = "host"
  memory      = 2048
  numa        = true
  hotplug     = "network,disk,usb,cpu,memory"
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"
  disk {
    slot     = 0
    size     = "5G"
    type     = "scsi"
    storage  = "local-lvm"
    iothread = 0
  }
  network {
    model  = "virtio"
    bridge = "vmbr0"
  }

#  ipconfig0 = "ip=192.168.99.21/24,gw=192.168.99.1"

  # Cloud-Init Drive
  ciuser  = "root"
  cipassword = "rootpass"
  nameserver = "8.8.8.8"
  sshkeys = <<EOF
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFg6kxv9uhOBBCoK4mIjpzz7OkQvi1c0rJF6kAIvTQ91
  EOF
  ipconfig0 = "ip=192.168.99.21/24,gw=192.168.99.1"
}
