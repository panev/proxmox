# Variable Definitions
variable "proxmox_api_url" {
    type = string
}

variable "proxmox_api_token_id" {
    type = string
}

variable "proxmox_api_token_secret" {
    type = string
    sensitive = true
}

variable "proxmox_node" {
  default = "pve02"
}

variable "proxmox_storage_pool" {
  default = "local-lvm"
}

variable "proxmox_storage_pool_type" {
  default = "lvm-thin"
}

variable "proxmox_storage_format" {
  default = "raw"
}

variable "proxmox_iso_pool" {
  default = "NFStorage:iso"
}

variable "iso_image" {
  default = "rocky8-x86_64-boot.iso"
}

variable "template_name" {
  default = "RL8-Template"
}

variable "template_description" {
  default = "#Rocky Linux 8 Template \n ### https://vbyte.org"
}

variable "vmid" {
    default = "9007"
}
variable "version" {
  default = ""
}

# Resource Definiation for the VM Template
source "proxmox" "rockylinux8" {
  # Proxmox Connection Settings
  proxmox_url = "${var.proxmox_api_url}"
  username = "${var.proxmox_api_token_id}"
  token = "${var.proxmox_api_token_secret}"
  # (Optional) Skip TLS Verification
  # insecure_skip_tls_verify = true
  node                        = "${var.proxmox_node}"

  # VM System Settings
  qemu_agent = true

  # PACKER Boot Commands
  boot_command                = [
      // "<tab> text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/inst.ks<enter><wait>"
      "<tab><bs><bs><bs><bs><bs>text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/inst.ks<enter><wait>"
      ]
  boot        = "c"
  boot_wait   = "5s"

  # PACKER Autoinstall Settings
  http_directory = "http"

  # VM Settings
  vm_id = "${var.vmid}"
  scsi_controller   = "virtio-scsi-pci"
  iso_file          = "${var.proxmox_iso_pool}/${var.iso_image}"
  sockets           = "1"
  cores             = "2"
  memory            = "2048"
  os                = "l26"
  cpu_type          = "host"
  ssh_username      = "root"
  ssh_password      = "Packer"
  ssh_port          = 22
  ssh_timeout       = "20m"
  unmount_iso       = true
  onboot            = true
  template_name     = "${var.template_name}"
  template_description = "${var.template_description}"

  # VM Hard Disk Settings
  disks {
    type = "virtio"
    disk_size = "8"
    storage_pool = "${var.proxmox_storage_pool}"
    storage_pool_type = "${var.proxmox_storage_pool_type}"
    format = "${var.proxmox_storage_format}"
  }
    # VM Network Settings
  network_adapters {
    model = "virtio"
    bridge = "vmbr0"
    // vlan_tag = ""
    firewall = "false"
  }
  # VM Cloud-Init Settings
  cloud_init = true
  cloud_init_storage_pool =  "local-lvm"

}

# Build Definition to create the VM Template
build {
    name = "rockylinux baby"
    sources = ["source.proxmox.rockylinux8"]

    provisioner "shell" {
        inline = [
            "yum install -y cloud-init qemu-guest-agent cloud-utils-growpart gdisk",
            "shred -u /etc/ssh/*_key /etc/ssh/*_key.pub",
            "rm -f /var/run/utmp",
            ">/var/log/lastlog",
            ">/var/log/wtmp",
            ">/var/log/btmp",
            "rm -rf /tmp/* /var/tmp/*",
            "unset HISTFILE; rm -rf /home/*/.*history /root/.*history",
            "rm -f /root/*ks",
            "passwd -d root",
            "passwd -l root"
        ]
    }
    # Enable QUEM agent
    provisioner "shell" {
        inline = [
            "echo Enable QUEM agent",
            "systemctl start qemu-guest-agent",
            "systemctl enable qemu-guest-agent"
        ]
    }
    # Provisioning the VM Template configuration #1
    provisioner "shell" {
        inline = [
            "echo Add Docker Container, zsh and git",
            "yum install epel-release -y",
            "yum config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
            "yum update -y",
            "yum -y install docker-ce docker-ce-cli containerd.io zsh git",
            "systemctl enable docker",
            "systemctl start docker",
            "curl -L https://github.com/bcicen/ctop/releases/download/v0.7.7/ctop-0.7.7-linux-amd64 > /usr/local/bin/ctop",
            "chmod +x /usr/local/bin/ctop"
        ]
    }
    # Provisioning the VM Template configuration #2
    provisioner "shell" {
        inline = [
            "echo install antigen zsh",
            "mkdir -p .antigen && curl -L git.io/antigen > .antigen/antigen.zsh",
            "usermod -s /usr/bin/zsh root"
        ]
    }
    provisioner "file" {
        source = "files/zshrc"
        destination = "~/.zshrc"
    }
}
