# Rocky Linux
packer build -var-file=credentials.pkr.hcl rocky8-server-docker.pkr.hcl
# Ubuntu
packer build -var-file=credentials.pkr.hcl ubuntu-server-jammy-docker.pkr.hcl
