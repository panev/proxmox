import requests
import json
import subprocess

### Environment Setup
VMIMG_ENV   = "./jammy-server-cloudimg-amd64.img"
VMMEM_ENV   = 2048
VMCPU_ENV   = 2
VMNAME_ENV  = "ubuntu-22.04-cloud"
VMSTR_ENV   = "local-lvm"

PVE_USER   = "your_username@pam"
PVE_PASS   = "password"
def get_cluster_resources(api_url, ticket):
    try:
        headers = {
            "Cookie": f"PVEAuthCookie={ticket}"
        }
        response = requests.get(api_url, headers=headers)
        response.raise_for_status()  # Raise an exception if the request was unsuccessful
        data = response.json()
        return data
    except requests.exceptions.RequestException as e:
        print("Error:", e)
        return None

# Specify the API endpoint URL
api_url = "https://pve.sysops.app:8006/api2/json/cluster/resources"

# Specify the authentication details
username = PVE_USER
password = PVE_PASS

# Get a new ticket
ticket_url = "https://pve.sysops.app:8006/api2/json/access/ticket"
ticket_payload = {
    "username": username,
    "password": password
}

try:
    ticket_response = requests.post(ticket_url, data=ticket_payload)
    ticket_response.raise_for_status()  # Raise an exception if the request was unsuccessful
    ticket_data = ticket_response.json()
    ticket = ticket_data["data"]["ticket"]
except requests.exceptions.RequestException as e:
    print("Error:", e)
    ticket = None

# Call the function to retrieve cluster resources
if ticket:
    resources_data = get_cluster_resources(api_url, ticket)

    # Check if the data retrieval was successful
    if resources_data:
        # Print the cluster resources in JSON format
#        print(json.dumps(resources_data, indent=4))

        # Extract the used VMIDs from the retrieved data
        used_vmids = []
        for item in resources_data["data"]:
            if "vmid" in item:
                used_vmids.append(item["vmid"])

        # Find the first free VMID after 9000
        vmid_range = range(9001, max(used_vmids) + 2)
        first_free_vmid = next((vmid for vmid in vmid_range if vmid not in used_vmids), None)

        if first_free_vmid is not None:
            print("The first free VMID found:", first_free_vmid)

            VMIMG  = VMIMG_ENV
            VMMEM  = VMMEM_ENV
            VMCPU  = VMCPU_ENV
            VMNAME = VMNAME_ENV
            VMSTR  = VMSTR_ENV

            # Create a new virtual machine
            print("Create a new virtual machine")
            subprocess.run(["qm", "create", str(first_free_vmid), "--memory", str(VMMEM), "--core", str(VMCPU), "--name", VMNAME, "--net0", "virtio,bridge=vmbr0", "--agent", "enabled=1", "--onboot", "1"])

            # Import the downloaded Cloud Image we downloaded before disk to the storage
            print("Import Cloud Image")
            subprocess.run(["qm", "importdisk", str(first_free_vmid), VMIMG, VMSTR])

            # Attach the new disk to the vm as a scsi drive on the scsi controller
            print("Attach the new disk")
            subprocess.run(["qm", "set", str(first_free_vmid), "--scsihw", "virtio-scsi-pci", "--scsi0", f"{VMSTR}:vm-{first_free_vmid}-disk-0"])

            # Add cloud init drive
            print("Add cloud init drive")
            subprocess.run(["qm", "set", str(first_free_vmid), "--ide2", f"{VMSTR}:cloudinit"])

            # Make the cloud init drive bootable and restrict BIOS to boot from disk only
            print("Make the cloud init drive bootable")
            subprocess.run(["qm", "set", str(first_free_vmid), "--boot", "c", "--bootdisk", "scsi0"])

            # Add serial console
            # subprocess.run(["qm", "set", str(first_free_vmid), "--serial0", "socket", "--vga", "serial0"])

            # Create a new cloud image template
            # subprocess.run(["qm", "template", str(first_free_vmid)])

        else:
            print("No free VMID available after 9000.")
    else:
        print("Failed to retrieve cluster resources.")
else:
    print("Ticket not available.")
